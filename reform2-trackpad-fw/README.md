# MNT Reform 2.0 Touchpad Firmware

## Code Structure

- `Mouse.c`: Sensor gesture processing + USB HID report generation
- `azoteq.{c,h}`: Azoteq sensor registers in structs for easy access
- `Descriptors.c`: USB HID descriptors

### Debian/Ubuntu

`apt install gcc-avr avr-libc dfu-programmer`

### Mac

*TODO: is this correct?*

```
brew tap osx-cross/avr
brew install avr-gcc
brew install dfu-programmer
```

## Building

Build the firmware by running `make`. The firmware can then be found in
Mouse.hex.

# Analysis

After building, you can load `Mouse.elf` in `gdb`, and inspect the generated code.
Try `disas main`, for example.

To flash, run:
`sudo ./flash.sh`

The script sends the 'xJTBL' command to the trackpad to send it into bootloader mode, and then flashes the new firmware.
The trackpad should reset briefly after flashing, and start running the new firmware.
