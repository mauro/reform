/*
  azoteq.c -- Azoteq IQS550 mappings
  Copyright 2023 Valtteri Koskivuori <vkoskiv@gmail.com>
  License: GPLv3
*/

#include "i2cmaster/i2cmaster.h"
#include "azoteq.h"

static void swap_u16(uint16_t *val) {
  uint8_t hi = (*val & 0xff00) >> 8;
  *val <<= 8;
  *val |= hi;
}

static void swap_16(int16_t *val) {
  uint8_t hi = (*val & 0xff00) >> 8;
  *val <<= 8;
  *val |= hi;
}

void read_azoteq_data(struct azoteq_data *data) {
  i2c_start_wait(ADDR_SENSOR|I2C_WRITE);
  i2c_write(0x00);
  i2c_write(0x00);
  i2c_rep_start(ADDR_SENSOR|I2C_READ);

  for (int i = 0; i < sizeof(*data); ++i) {
    ((unsigned char *)data)[i] = i2c_readAck();
  }
  i2c_readNak();
  i2c_stop();

  // end cycle
  i2c_start_wait(ADDR_SENSOR|I2C_WRITE);
  i2c_write(0xee);
  i2c_write(0xee);
  i2c_write(0xff);
  i2c_stop();

  // Sad, we have to swap byte order for 16-bit quantities
  for (int i = 0; i < 5; ++i) {
    swap_u16(&data->fingers[i].abs_x);
    swap_u16(&data->fingers[i].abs_y);
  }
  swap_16(&data->relative_x);
  swap_16(&data->relative_y);
}

